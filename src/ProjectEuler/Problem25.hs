module ProjectEuler.Problem25 (problem25) where

import Math.Sets
import ProjectEuler.Utils

number = 25

title = ""

description = ""

-- (+2) because fibonacci produces 1,2,3,...
solution = (+) . length .takeWhile ((<1000) . length . toDigits) $ fibonacci

problem25 = Problem number title description solution
