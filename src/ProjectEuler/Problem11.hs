module ProjectEuler.Problem11 (problem11) where

import ProjectEuler.Utils
import Data.List

number = 11

title = "Largest product in a grid"

description = "Int the 20x20 grid below, four numbers along a diagonal line have been marked in red.\n\
\n\
\the product of these numbers is 26x 63 x 78 x 14 = 1788696.\n\
\n\
\What is the greatest product of four adjacent numbers in the same direction (up, down, left, right or diagonally) in the 20x20 grid?\n"

solution = -1

problem11 = Problem number title description solution

rotate90 = reverse . transpose
rotate180 = rotate90 . rotate90
rotate180' = map reverse . reverse

diagonals = (++) <$> transpose . zipWith drop [0..]
                 <*> transpose . zipWith drop [1..] . rotate180'
