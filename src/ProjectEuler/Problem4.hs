module ProjectEuler.Problem4 (solution4) where

import qualified Math.Miscellaneous as Misc

solution4 :: Integer
solution4 = maximum [x*y | x <- [100 .. 999], y <- [100 .. 999], Misc.isPalindrome . Misc.toDigits $ (x*y)]
