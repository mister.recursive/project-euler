module ProjectEuler.Problem5 (solution5) where

solution5 :: Integer
solution5 = foldl1 lcm [1 .. 20]

