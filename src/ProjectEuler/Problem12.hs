module ProjectEuler.Problem12 (problem12) where

import ProjectEuler.Utils

number = 12

title = "Large Sum"

description = "Work out the first ten digits of the sum of the following one-hundred 50-digit numbers."

solution = -1

problem = Problem number title description solution
