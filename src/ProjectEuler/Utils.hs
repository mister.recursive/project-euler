module ProjectEuler.Utils where

type Number = Int
type Title = String
type Description = String
type Solution = Integer


data Problem = Problem  Number Title Description Solution

instance Show Problem where
    show (Problem n t d s) = "Problem "
                   ++ show n
                   ++ "\n\n"
                   ++  t
                   ++ "\n\n"
                   ++ d
                   ++ "\n\n"
                   ++ "The solution is: "
                   ++ show s
