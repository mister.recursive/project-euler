module ProjectEuler.Problem1 (problem1) where

import ProjectEuler.Utils

number = 1

title = "Multiples of 3 and 5"


description = "If we list all the natural numbers below 10 that are multiples of 3 or 5,\n\
              \we get 3, 5, 6 and 9. The sum of theses multiples is 23.\n\
              \Find the sum of all the multiples of 3 or 5 below 1000.\n"


solution = sum [x | x <- [ 1 .. 999], mod x 3 == 0 || mod x 5 == 0 ]

problem1 = Problem number title description solution
