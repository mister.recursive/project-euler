module ProjectEuler.Problem3 (problem3) where

--Largest prime factor
--
--The prime factors of 13195 are 5, 7, 13 and 29.
--
--What is the largest prime factor of the number 600851475143 ?

import ProjectEuler.Utils
import qualified Math.NumberTheory as NT

number = 3

title = "Largest prime factor"

description = "he prime factors of 13195 are 5, 7, 13 and 29.\n\n\
              \What is the largest prime factor of the number 600851475143?"

solution = maximum $ NT.primeFactors 600851475143

problem3 = Problem number title description solution
