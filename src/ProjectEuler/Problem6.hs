module ProjectEuler.Problem6 (solution6) where


sumOfSquares :: Integer -> Integer
sumOfSquares n = div (n*(n+1)*(2*n+1)) 6

squareOfSum :: Integer -> Integer
squareOfSum n =  div (n*(n+1)) 2 ^ 2


solution6 :: Integer
solution6 = squareOfSum 100 - sumOfSquares 100
