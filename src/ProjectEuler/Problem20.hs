module ProjectEuler.Problem20 (problem20) where

import qualified Math.Miscellaneous
import ProjectEuler.Utils


number = 20

title = ""

description = ""

solution = sum . Math.Miscellaneous.toDigits . product $ [1..100]

problem20 = Problem number title description solution
