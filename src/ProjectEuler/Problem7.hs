module ProjectEuler.Problem7 (solution7) where

import qualified Math.Sets as Sets

solution7 :: Integer
solution7 = Sets.primes !! 10000
