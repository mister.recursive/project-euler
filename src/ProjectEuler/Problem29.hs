module ProjectEuler.Problem29 (problem29) where

import qualified Data.Set as S
import ProjectEuler.Utils

number = 29

title = ""

description = ""

solution = fromIntegral . S.size . S.fromList $ [a^b | a <- [2..100], b <- [2..100]]

problem29 = Problem number title description solution
