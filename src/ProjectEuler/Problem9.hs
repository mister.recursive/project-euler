module ProjectEuler.Problem9 (problem9) where

import ProjectEuler.Utils
import Math.Sets
import Math.Miscellaneous

number = 9

title = "Special Pythagorean triplet"

description = "A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,\n\n\
              \a^2 + b^2 = c^2\n\n\
              \For example, 32 + 42 = 9 + 16 = 25 = 52.\n\n\
              \There exists exactly one Pythagorean triplet for which a + b + c = 1000.\n\
              \Find the product abc."

solution = undefined


problem9 :: Problem
problem9 = Problem number title description solution
