module ProjectEuler.Problem10 (problem10) where

import ProjectEuler.Utils
import Math.Sets
import Math.Miscellaneous


number = 10

title = "Summation of primes"

description = "The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.\n\n\
              \find the sum of all the primes below two million.\n"

solution = (sum ... takeWhile) (<2000000) primes

problem10 =  Problem number title description solution
