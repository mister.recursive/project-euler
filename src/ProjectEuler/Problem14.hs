module ProjectEuler.Problem14 (problem14) where

import Math.Sets
import           ProjectEuler.Utils
import qualified Data.Vector        as V

number = 14

title = "Longest Collatz sequence"

description = ""

solution = V.maxIndex . V.fromList . map (length . collatzSequence) $ [1 .. 999999]

problem14 = Problem number title description solution
