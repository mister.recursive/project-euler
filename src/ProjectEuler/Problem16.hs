module ProjectEuler.Problem16 (problem16) where

import ProjectEuler.Utils
import qualified Math.Miscellaneous

number = 16

title = "Power digit sum"

description = ""

solution = sum . Math.Miscellaneous.toDigits $ 2^1000

problem16 = Problem number title description solution
